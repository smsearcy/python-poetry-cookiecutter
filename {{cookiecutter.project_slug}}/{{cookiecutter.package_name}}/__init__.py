{% if cookiecutter.python_version >= "3.8" %}
from importlib.metadata import version
{% else %}
try:
    from importlib.metadata import version
except ImportError:
    # use backport of Python 3.8 library
    from importlib_metadata import version  # type: ignore
{% endif %}

__version__ = version("{{ cookiecutter.project_slug }}")
