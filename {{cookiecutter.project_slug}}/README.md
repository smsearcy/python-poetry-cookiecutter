# {{ cookiecutter.project_name }}

{{ cookiecutter.project_short_description }}


## Developing

`{{ cookiecutter.project_slug }}` uses [Poetry](https://python-poetry.org/) so you will
need that installed and available in your path.  Once you have that:

1. Fork/clone the Git repository via your preferred tool and `cd` to that directory in a
terminal.
2. If you have multiple Python versions installed run `poetry env use "<path to python>"`
or create/activate a virtual environment prior to running Poetry.
3. To create a virtualenv and install the package with its dependencies (including
development dependencies) run `poetry install`.  *If running in a virtual environment
already then it will use that virtual environment.*
4. To configure your IDE to use the correct virtual environment run `poetry env info` and
update your IDE to use the Virtualenv path specified.
5. To run the command line scripts (defined in `pyproject.toml`) run `poetry run {command}`.
