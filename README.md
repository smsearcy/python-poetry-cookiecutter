# python-poetry-cookiecutter

[Cookiecutter](https://github.com/audreyr/cookiecutter) template for a Python
application.


## Features

* Uses [Poetry](https://python-poetry.org/) for dependency management instead of
  `setup.py` (aka [Setuputils](https://setuptools.readthedocs.io/)).
* Unit testing via [pytest](http://pytest.org/).
* Code formatting via [pre-commit](https://pre-commit.com/) and
  [Black](https://black.readthedocs.io/).
* [GitLab CI](https://gitlab.com/) for CI.
* [Tox](https://tox.readthedocs.io/) for various other consistent tests.

**Note:** The interaction between Poetry and Tox is something that is not
necessarily optimized yet.


## Quickstart

Generate a Python package project (assuming Cookiecutter is installed)::

    cookiecutter https://gitlab.com/smsearcy/python-poetry-cookiecutter


## Prompts

An explanation of the information you will be prompted for when generating the
Python package.

### Templated Values

The following appear in various parts of your generated project.

* `author`: Full name of author for project metadata.
* `email`: Author's email for project metadata.
* `username`: Username for building repository URL.
* `project_name`: The name of your new Python package project. This is used in
documentation, so spaces and any characters are fine here.
* `project_slug`: The name used for the Git repository and PyPI package name.
Typically, it is the slugified version of `project_name`.
* `package_name`: The name of your new Python package, needs to be a valid
Python name.
* `project_short_description`: A 1-sentence description of what your Python
package does.
* `project_uri`: URI for the project repository.
* `python_version`: Minimum Python version to support.
* `version`: The starting version number of the package.
